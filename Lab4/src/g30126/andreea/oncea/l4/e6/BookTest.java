package g30126.andreea.oncea.l4.e6;

import static org.junit.Assert.*;

import org.junit.Test;

import g30126.andreea.oncea.l4.e4.Author;

public class BookTest {

	@Test
	public void testToString() {
		Author[] a;
		Book b = new Book("Morometii",a= new Author[3],10.15);
		a[0] = new Author("Preda","Preda@yahoo.com",'m');
		a[1] = new Author("Slavici","Slavici@gmail.com",'m');
		a[2] = new Author("Rebreanu","Rebreanu@yahoo.com",'m');
		assertEquals("book-Morometii by 3 authors",b.toString());
	}
	
	@Test
	public void testPrintAuthors() {
		Author[] a;
		Book b = new Book("Morometii",a= new Author[3],10.15);
		a[0] = new Author("Preda","Preda@yahoo.com",'m');
		a[1] = new Author("Slavici","Slavici@gmail.com",'f');
		a[2] = new Author("Rebreanu","Rebreanu@yahoo.com",'m');
		assertEquals("Preda",a[0].getName());
		assertEquals("Slavici",a[1].getName());
		assertEquals("Rebreanu",a[2].getName());
		
	}

}

