package g30126.andreea.oncea.l4.e6;

import g30126.andreea.oncea.l4.e4.Author;

public class Book {
	private String name;
	private Author[] author;
	private double price;
	private int qtyInStock;
	public Book (String name, Author[] author, double price)
		{
			this.name=name;
			this.author=author;
			this.price=price;
			qtyInStock=0;
		}
	public Book (String name, Author[] author, double price, int qtyInStock)
		{
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
		}
	public String getName()
		{
			return name;
		}
	public Author[] getAuthor()
		{
			return author;
		}
	public double getPrice ()
		{
			return price;
		}
	public int getQtyInStock()
		{
			return qtyInStock;
		}
	public void setPrice(double price)
		{
			this.price=price;
		}
	public void setQtyInStock( int qtyInStock)
		{
			this.qtyInStock=qtyInStock;
		}
	public String toString()
		{
			String message="book-"+name+" by "+author.length+" authors";
			return message;
		}
	public void printAuthors()
		{
			int i;
			for(i=0;i<author.length;i++)
				{
					System.out.println(author[i].getName());
				}
		}
	public static void main(String[] args) {
		

	}

}
