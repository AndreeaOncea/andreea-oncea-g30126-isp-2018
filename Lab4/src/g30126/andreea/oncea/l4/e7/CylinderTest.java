package g30126.andreea.oncea.l4.e7;

import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {

	@Test
	public void testGetArea() {
		Cylinder c = new Cylinder(1,1);
		assertEquals(4*Math.PI,c.getArea(),0.01);
	}

	@Test
	public void testGetVolume() {
		Cylinder c = new Cylinder(1,1);
		assertEquals(Math.PI,c.getVolume(),0.01);
	}

}

