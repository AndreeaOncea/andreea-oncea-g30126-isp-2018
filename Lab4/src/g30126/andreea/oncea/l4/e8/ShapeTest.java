package g30126.andreea.oncea.l4.e8;

import static org.junit.Assert.*;

import org.junit.Test;

public class ShapeTest {

	@Test
	public void testToString() {
		Shape s= new Shape();
		assertEquals("A Shape with color of green and filled true",s.toString());
	}

}

