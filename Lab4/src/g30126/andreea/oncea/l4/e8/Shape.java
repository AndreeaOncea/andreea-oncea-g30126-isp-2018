package g30126.andreea.oncea.l4.e8;

public class Shape {
	private String color;
	private Boolean filled;
	public Shape ()
		{
			color="green";
			filled=true;
		}
	public Shape (String color, Boolean filled)
		{
			this.color=color;
			this.filled=filled;
		}
	public String getColor ()
		{
			return color;
		}
	public Boolean isFilled()
		{
			return filled;
		}
	public void setColor (String color)
		{
			this.color=color;
		}
	public void setFilled (Boolean filled)
		{
			this.filled=filled;
		}
	public String toString()
		{
			return "A Shape with color of " + color + " and filled "+filled;
		}
public static void main (String[] args) {
		
	}
 }


