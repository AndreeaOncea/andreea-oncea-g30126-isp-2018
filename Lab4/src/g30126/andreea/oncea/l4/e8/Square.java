package g30126.andreea.oncea.l4.e8;

public class Square extends Rectangle{
	public Square()
		{
			super();
		}
	public Square(double side)
		{
			super(side,side);
		}
	public Square(double side, String color, Boolean filled)
		{
			super(side,side,color,filled);
		}
	public double getSide()
		{
			return getLength();
		}
	public void setSide(double side)
		{
			setLength(side);
			setWidth(side);
		}
	public void setWidth(double side)
		{
			setWidth(side);
		}
	public void setLength(double side)
		{
			setLength(side);
		}
	public String toString()
		{	
			return "A Square with side="+getSide()+", which is a subclass of "+super.toString();
		}
	public static void main (String[] args)
	{
		Square s = new Square();
		System.out.println(s.toString());
	}
}
