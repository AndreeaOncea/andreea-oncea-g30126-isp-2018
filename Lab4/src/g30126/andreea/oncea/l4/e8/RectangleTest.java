package g30126.andreea.oncea.l4.e8;

import static org.junit.Assert.*;

import org.junit.Test;

public class RectangleTest {

	@Test
	public void testToString() {
		Rectangle r = new Rectangle();
		assertEquals("A Rectangle with width=1.0 and length=1.0, which is a subclass of A Shape with color of green and filled true",r.toString());
	}

	@Test
	public void testGetArea() {
		Rectangle r = new Rectangle();
		assertEquals(1,r.getArea(),0.01);
	}

	@Test
	public void testGetPerimeter() {
		Rectangle r = new Rectangle();
		assertEquals(4,r.getPerimeter(),0.01);
	}

}
