package g30126.andreea.oncea.l4.e5;

import static org.junit.Assert.*;

import org.junit.Test;

import g30126.andreea.oncea.l4.e4.Author;

public class BookTest {

	@Test
	public void testToString() {
		Author a = new Author("Liviu Rebreanu","LiviuRebreanu@gmail.com",'m');
		Book b = new Book("Ion",a,20.99);
		assertEquals("'book-Ion' by author-Liviu Rebreanu (m) at LiviuRebreanu@gmail.com",b.toString());
		
	}

}

