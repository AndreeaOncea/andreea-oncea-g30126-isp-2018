package g30126.oncea.andreea.l8.e2;

import java.io.FileReader;
import java.io.IOException;

public class Main {
	
public static void main(String args[]) {
            int nr=0;
            try(FileReader fr = new FileReader("D://data.txt")) {   //crearea unui flux de intrare pe caractere
                char [] a = new char[50];
                fr.read(a);   // reads the content of the array
                for(char c : a)
                    if (c=='e')
                    nr++;
                    System.out.print("The number of times character 'e' appears in the file is: "+nr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
}


