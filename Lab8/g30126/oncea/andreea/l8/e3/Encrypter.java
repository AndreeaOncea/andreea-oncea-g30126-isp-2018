package g30126.oncea.andreea.l8.e3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Encrypter {
	private String operation;
	private String fileName;
	private Scanner s;

	public Encrypter() {
		s = new Scanner(System.in);
		operation = s.next();
		fileName = s.next();
	}

	public String getOperation() {
		return operation;
	}

	public void encrypt() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String s, s1 = new String();
		while ((s = br.readLine()) != null) {
			s1 += s;
		}
		br.close();
		String fname = "text" + ".enc";
		BufferedWriter bw = new BufferedWriter(new FileWriter(fname));
		String s2 = new String();
		for (int i = 0; i < s1.length(); i++) {
			int a = s1.charAt(i);
			a--;
			char b = (char) a;
			s2 += b;
		}
		bw.write(s2);
		bw.close();
	}

	public void decrypt() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String s, s1 = new String();
		while ((s = br.readLine()) != null) {
			s1 += s;
		}
		br.close();
		String fname = "text" + ".dec";
		BufferedWriter bw = new BufferedWriter(new FileWriter(fname));
		String s2 = new String();
		for (int i = 0; i < s1.length(); i++) {
			int a = s1.charAt(i);
			a++;
			char b = (char) a;
			s2 += b;
		}
		bw.write(s2);
		bw.close();
	}

	public static void main(String[] args) throws IOException {
		Encrypter e = new Encrypter();
		String s = new String();
		s = e.getOperation();
		if (s.equals("encrypt"))
			e.encrypt();
		else if (s.equals("decrypt"))
			e.decrypt();
	}
}
