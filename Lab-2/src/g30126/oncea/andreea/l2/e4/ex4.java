package g30126.oncea.andreea.l2.e4;

import java.util.Random;
import java.util.Scanner;


public class ex4 {
    public static void main(String[] args) {

        Random r = new Random();
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        
        int[] vec = new int[x];
        
        for (int i = 0; i < vec.length; i++) {
            vec[i] = r.nextInt(200);
            System.out.print(vec[i] + " ");
        }
        System.out.print("\n");
        System.out.println(GetMax(vec));
        
    }

    private static int GetMax(int[] vec) {
        int max = vec[0];
        for (int i = 0; i < vec.length; i++) {
           if (vec[i] > max) 
               max = vec[i];
        }
        return max;
    }
}
