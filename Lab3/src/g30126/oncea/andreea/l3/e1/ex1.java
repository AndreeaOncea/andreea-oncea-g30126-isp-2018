package g30126.oncea.andreea.l3.e1;
import becker.robots.*;

public class ex1
{
   public static void main(String[] args)
   {  
   	// Set up the initial situation
   	City prague = new City();
      Thing parcel = new Thing(prague, 1, 2);
      Robot Rob = new Robot(prague, 1, 0, Direction.EAST);
 
		// Direct the robot to the final situation
      Rob.move();
      Rob.move();
      Rob.pickThing();
      Rob.move();
      Rob.turnLeft();	// start turning right as three turn lefts
      Rob.turnLeft();
      Rob.turnLeft();	// finished turning right
      Rob.move();
      Rob.putThing();
      Rob.move();
   }
}
