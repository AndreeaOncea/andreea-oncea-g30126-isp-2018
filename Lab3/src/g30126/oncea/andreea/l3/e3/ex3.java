package g30126.oncea.andreea.l3.e3;

//Write a program that creates a robot at (1, 1) that moves north five times, turns around, and returns to its starting point.

import becker.robots.*;

public class ex3 {
	public static void main (String[] args) {
		 // Set up the initial situation
		  City bucharest = new City();
	      //Thing parcel = new Thing(bucharest, 1, 2);
	      Robot Bob = new Robot(bucharest, 1, 1, Direction.NORTH);
	 
			// Direct the robot to the final situation

	      Bob.move();
	      Bob.move();
	      Bob.move();
	      Bob.move();
	      Bob.move();
	      Bob.turnLeft();
	      Bob.turnLeft();
	      Bob.move();
	      Bob.move();
	      Bob.move();
	      Bob.move();
	      Bob.move();
}
}
