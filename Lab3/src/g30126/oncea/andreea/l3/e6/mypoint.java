package g30126.oncea.andreea.l3.e6;

public class mypoint {

    private int x;
    private int y;

    public mypoint() {
        x = 0;
        y = 0;
    }

    public mypoint(int a, int b) {
        this.x = a;
        this.y = b;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y) {
        setX(x);
        setY(y);
    }

    public String toString() {
        String instance;
        instance = "(" + this.x + "," + this.y + ")";
        return instance;
    }

    public double distance(int x, int y) {
        return Math.hypot(x-this.x, y-this.y);
    }

    public double OverloadDistance(mypoint p) {
        return Math.hypot(p.x-this.x, p.y-this.y);
    }
}
