package g30126.oncea.andreea.l3.e5;

import becker.robots.*;

public class ex5 {
	public static void main (String[] args) {
		City oradea = new City();
		Thing parcel = new Thing(oradea, 2, 2);
		Wall o0 = new Wall(oradea, 1, 1, Direction.WEST);
		Wall o1 = new Wall(oradea, 2, 1, Direction.WEST);
		Wall o2 = new Wall(oradea, 1, 2, Direction.EAST);
		Wall o3 = new Wall(oradea, 1, 1, Direction.NORTH);
		Wall o4 = new Wall(oradea, 1, 2, Direction.NORTH);
		Wall o5 = new Wall(oradea, 2, 1, Direction.SOUTH);
		Wall o6 = new Wall(oradea, 1, 2, Direction.SOUTH);
		Robot Robbi = new Robot(oradea, 1, 2, Direction.SOUTH);

		// Direct the robot to the final situation
		Robbi.turnLeft();
		Robbi.turnLeft();
		Robbi.turnLeft();
		Robbi.move();
		Robbi.turnLeft();
		Robbi.move();
		Robbi.turnLeft();
		Robbi.move();
		Robbi.pickThing();
		Robbi.turnLeft();
		Robbi.turnLeft();
		Robbi.move();
		Robbi.turnLeft();
		Robbi.turnLeft();
		Robbi.turnLeft();
		Robbi.move();
		Robbi.turnLeft();
		Robbi.turnLeft();
		Robbi.turnLeft();
		Robbi.move();
		Robbi.turnLeft();
		Robbi.turnLeft();
		Robbi.turnLeft();
	}	

}
