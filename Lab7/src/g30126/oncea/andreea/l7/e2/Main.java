package g30126.oncea.andreea.l7.e2;

public class Main {
	
	public static void main(String[] args) {
		Bank bank = new Bank();
		bank.addAccount("Ana", 109.98);
		bank.addAccount("Ava", 111.97);
		bank.addAccount("Dan", 209.88);
		bank.addAccount("Carla", 123.55);
		
		bank.printAccounts();
		bank.printAccounts(51,120);
		bank.getAccount("Ema");
		bank.getAllAccounts();
	}

}
