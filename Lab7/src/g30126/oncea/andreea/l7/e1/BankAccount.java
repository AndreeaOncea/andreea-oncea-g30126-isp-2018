package g30126.oncea.andreea.l7.e1;
import java.util.Objects;

public class BankAccount{
	    private String owner;
	    private double balance;
		public Object getOwner;
		
		public String getOwner()
		{
			return owner;
		}
		public void setOwner(String owner) {
	        this.owner = owner;
	    }
		 public void setBalance(double balance) {
		        this.balance = balance;
		    }

		public double getBalance()
		{
			return balance;
		}


	    public void withdraw(double amount) {
	        balance -= amount;
	    }
	    
	    public void deposit(double amount) {
	    
	        balance += amount;
	    }

	  public BankAccount(String owner,double balance){
		  this.owner=owner;
		  this.balance=balance;
	  }
	  
        @Override
    	public boolean equals(Object obj) {
    		if(obj instanceof BankAccount){
    			BankAccount b = (BankAccount)obj;
    			return balance == b.balance;
    		}
    		return false;
    	}
        
        public int hashCode(){
            return (int) (owner.hashCode()+balance);
        }

        public static void main(String args[]){
        	BankAccount b1=new BankAccount("Andreea", 134);
        	BankAccount b2=new BankAccount("Andrei", 133);
        	
        	if (b1.equals(b2)) 
        		  System.out.println(b1+" and "+b2+" are equals");
        		else
        	       System.out.println(b1+" and "+b2+ " are not equals");	
        		
        	if(b1.owner.equals(b2.owner))
        		System.out.println(b1+" and "+b2+" have the same owner");
        	
        	
        }
        
	  
	}
