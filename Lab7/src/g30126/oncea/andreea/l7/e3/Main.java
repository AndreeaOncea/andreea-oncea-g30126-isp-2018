package g30126.oncea.andreea.l7.e3;


import g30126.oncea.andreea.l7.e3.Bank;

public class Main {
	
	public static void main(String[] args) {
		Bank bank = new Bank();
		bank.addAccount("Dan", 109.98);
		bank.addAccount("Mara", 111.97);
		bank.addAccount("Ana", 209.88);
		bank.addAccount("Ema", 123.55);
		
		bank.printAccounts();
		bank.printAccounts(51,120);
		bank.getAccount("Ema");
		bank.getAllAccounts();
	}

}
