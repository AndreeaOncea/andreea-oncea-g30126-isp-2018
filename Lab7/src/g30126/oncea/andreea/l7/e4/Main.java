package g30126.oncea.andreea.l7.e4;

public class Main {
	
	public static void main(String[] args) {
		Dictionary d = new Dictionary();
		Word cuv = new Word("rosu");
		Definition def = new Definition("culoare");		
		Word cuv1 = new Word("vrabie");
		Definition def1 = new Definition("pasare");	
		Word cuv2 = new Word("somn");
		Definition def2 = new Definition("peste");	
		d.addWord(cuv,def);
		d.addWord(cuv1,def1);
		d.addWord(cuv2,def2);
		
		d.getAllDefinition();
		d.getDefinition(cuv);
		d.getAllWords();
	}
}

