package g30126.andreea.oncea.l5.e3;

public class TemperatureSensor extends Sensor {
	
	int valueT;
	public TemperatureSensor() {
		this.valueT=(int)(Math.random()*100);
	}
	public int readValue() {
		return this.valueT;
	}
}

