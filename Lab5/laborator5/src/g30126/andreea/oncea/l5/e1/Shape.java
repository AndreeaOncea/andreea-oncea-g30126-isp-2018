package g30126.andreea.oncea.l5.e1;

public abstract class Shape {
	protected String color;
	protected boolean filled;
	
	public Shape(){
		}
	
	public Shape(String color,boolean filled){
		
	    this.color=color;
	    this.filled=filled;
	}
	
	public String getColor(){
		
		return color;
	}
	
	public void setColor(String color){
		this.color=color;
	}
	
	public boolean isFilled(){
		return filled;
	}
	
	public void setFilled(boolean filled){
		this.filled=filled;
		}
	public abstract double getArea();
	
	public abstract double getPerimeter();
	
	@Override
	public String toString() {
		return "Shape{" +
				"color='" + color + '\'' +
	            ", filled=" + filled +
	            '}';
	     }
	
	public static void main (String[] args) {
	   	Shape[] s = new Shape[3];
	   	s[0] = new Circle(1);
	   	s[1] = new Rectangle(3,2);
	   	s[2] = new Square(2);
	   	for(int i = 0; i < s.length; i++ )
	   		{
	   			System.out.println(s[i].getArea());
	   			System.out.println(s[i].getPerimeter());
	   		}
   }
}

	
	
	
	


