package g30126.andreea.oncea.l5.e1;

public class Rectangle extends Shape {
	
   protected double width;
   protected double length;
   
   public Rectangle(){
	   width=1.0;
	   length=1.0;
	 }
   
   public Rectangle(double width, double length){
	   this.width=width;
	   this.length=length;
   }
   
   public Rectangle(double width, double length, String color,boolean filled){
	   super(color,filled);
	   this.width=width;
	   this.length=length;
   }
   
   public double getWidth(){ 
	   return width;
   }
   
   public void setWidth(){
	   this.width=width;
   }
   
   public double getLength(){ 
	   return length;
   }
   
   public void setLength(){
	   this.length=length;
   }
   
  public double getArea(){
	  return width*length;
	  
  }
   public double getPerimeter(){
	   return 2*width+2*length;
   }
   
   @Override
   public String toString() {
       return "Rectangle{" +
               "color='" + color + '\'' +
               ", filled=" + filled +
               ", width=" + width + ", length="+ length+
               '}';
 }
}
