package g30126.andreea.oncea.l5.e1;

public class Square extends Rectangle{
	public Square()
		{
			super();
		}
	public Square(double side)
		{
			super(side,side);
		}
	public Square(double side, String color, Boolean filled)
		{
			super(side,side,color,filled);
		}
	public double getSide()
		{
			return getLength();
		}
	public void setSide(double side)
		{
			super.setLength();
			super.setWidth();
		}
	public void setWidth(double side)
		{
			super.setWidth();
		}
	public void setLength(double side)
		{
			super.setLength();
		}
		@Override
		   public String toString() {
		       return "Rectangle{" +
		               "color='" + color + '\'' +
		               ", filled=" + filled +
		               ", width=" + width + ", length="+ length+
		               '}';
		}
}
