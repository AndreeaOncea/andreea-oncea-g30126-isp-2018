package g30126.andreea.oncea.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle implements Shape {
	private int length;
    private int width;
    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;
    
    public Rectangle(Color color, int x, int y, int length, int width,String id,boolean fill) {
        
    	
        this.color=color;
        this.x=x;
        this.y=y;
        this.length = length;
        this.width = width;
        this.id=id;
        this.fill=fill;
    }

    
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	 
    public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}


	public void draw(Graphics g) {
		// TODO Auto-generated method stub
		System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), getLength(), getWidth());
        if(isFill()==true)
        {
        	g.fillOval(getX(), getY(), width, length);
        }
	}
	
   /* public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), getLength(), getWidth());
        if(isFill()==true)
        {
        	g.fillOval(getX(), getY(), width, length);
        }
    }
	*/
	

}

