package g30126.andreea.oncea.l6.e3;

import java.awt.*;

public interface Shape {
	public Color getColor();
	public void setColor(Color color);
	public int getX();
	public void setX(int x);
	public int getY();
	public void setY(int y);
	public String getId();
	public void setId(String id);
	public boolean isFill();
	public void setFill(boolean fill);
	
	public void draw(Graphics g);



}
