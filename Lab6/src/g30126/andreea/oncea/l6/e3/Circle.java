package g30126.andreea.oncea.l6.e3;

import java.awt.*;
import java.awt.Graphics;

public class Circle implements Shape {
	
	private int radius;
	private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;
    
    public Circle(Color color, int x, int y,int radius,String id,boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        
        this.radius=radius;
        this.id=id;
        this.fill=fill;
    } 
    public int getRadius() {
        return radius;
    }
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	 
    public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
 
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(isFill()==true)
        {
        	g.fillOval(getX(), getY(), radius, radius);
        }
    }
	
    
    
}

