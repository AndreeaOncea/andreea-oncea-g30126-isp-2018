package g30126.andreea.oncea.l6.e1;

import java.awt.*;

public abstract class Shape {

    public Color color;
    public int x,y;
    public String id;
    public boolean fill;
    

    public Shape(Color color, int x,int y,String id,boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
    public int getX(){
    	return x;
    }
    
    public void setX(int x){
    	this.x=x;
    }
    
    public int getY(){
    	return y;
    }
    
    public void setY(int y){
    	this.y=y;
    }
    public String getId(){
    	return id;
    }
    
    public void setId(String id){
    	this.id=id;
    }
    
    public boolean isFill() {
    	return fill;
    }  
    public void setFill(boolean fill){
    	this.fill=fill;
    }
    public abstract void draw(Graphics g);

	public void draw1(Graphics g) {
		// TODO Auto-generated method stub
		
	}
}
